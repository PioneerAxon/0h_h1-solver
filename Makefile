CC=gcc
CFLAGS=-Wall -O3
LDFLAGS=

0h_h1-solver:board.o solver.c
	$(CC) $(CFLAGS) $? -o $@

clean:
	rm -rf 0h_h1-solver *.o
